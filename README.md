# Willow-SpinKit

![GitHub release](https://img.shields.io/github/release/ba-st/Willow-SpinKit.svg)
[![Build Status](https://travis-ci.org/ba-st/Willow-SpinKit.svg?branch=master)](https://travis-ci.org/ba-st/Willow-SpinKit)
[![Coverage Status](https://coveralls.io/repos/github/ba-st/Willow-SpinKit/badge.svg?branch=master)](https://coveralls.io/github/ba-st/Willow-SpinKit?branch=master)
[![Javascript Dependency Status](https://david-dm.org/ba-st/Willow-SpinKit.svg)](https://david-dm.org/ba-st/Willow-SpinKit)

*Integration between [Willow](https://github.com/ba-st/Willow) and [SpinKit](https://github.com/tobiasahlin/SpinKit)*

## Goals
- Offer a set of loading notifications for your web applications when developing with Willow

### License:
The project source code is [MIT](LICENSE) licensed. Any contribution submitted to the code repository is considered to be under the same license.

The documentation is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

### Highlights:
- **Supported Platforms**: [Pharo 5 / Pharo 6](http://www.pharo.org/)
- **Source Code Repository** and **Issue Tracking**: In this GitHub repository.

### Get started!

#### Pharo 6/7

Open a Playground and evaluate:

```smalltalk
Metacello new
  baseline: 'WillowSpinKit';
  repository: 'github://ba-st/Willow-SpinKit:master/source';
  load
```
